import logging
from os import listdir
from typing import List

import pandas as pd
from psycopg2 import connect

from .config import Config

config = Config(".env")
log = logging.getLogger(__name__)

# import pandas as pd
# pd.set_option('display.max_columns', None)
# pd.set_option('display.max_rows', None)

# Settings

CLEANED_DATA_PATH = config(
    'CLEANED_DATA_PATH', cast=str, default='./cleaned_data/')

# list of years to filter by, default to empty list to get all years
FILTER_YEARS = config(
    'FILTER_YEARS', cast=list, default=[])

# Database Settings

POSTGRES_HOST = config('POSTGRES_HOST', cast=str, default='localhost')
POSTGRES_DATABASE = config(
    'POSTGRES_DATABASE', cast=str, default='postgres')
POSTGRES_USER = config('POSTGRES_USER', cast=str, default='root')
POSTGRES_PASSWORD = config('POSTGRES_PASSWORD', cast=str, default='doot')

CI_ENVIRONMENT = config('CI', cast=bool, default=False)
CONNECT_DB = config('CONNECT_DB', cast=bool, default=True)


def get_connection():
    """
    Insatiate a database connection
    """
    connection = connect(
        host=POSTGRES_HOST,
        database=POSTGRES_DATABASE,
        juser=POSTGRES_USER,
        password=POSTGRES_PASSWORD,
    )
    connection.autocommit = True
    return connection


def find_filenames(path_to_dir: str, suffix: str = ".csv") -> List[str]:
    """
    get a list of filenames given a directory path, and a suffix to match on,
    defaults to csv. equivalent to `ls *.csv`
    """
    filenames = listdir(path_to_dir)
    return [filename for filename in filenames if filename.endswith(suffix)]


def filter_by_year(
        filenames: List[str],
        years: list = FILTER_YEARS) -> List[str]:
    """
    filters filenames by a given year, for a list of years

    years is actually List[str] but it conflicts with the config casting it
    """
    for year in years:
        for f in filenames:
            if year in f:
                filenames.remove(f)
    return filenames


def parse_csv(filepath: str):
    print(f'parssing csv {filepath}')
    return pd.read_csv(filepath)


def create_mortality_table(cursor) -> None:
    cursor.execute("""
        DROP TABLE IF EXISTS mortality;
        CREATE TABLE mortality(
  resident_status INT,
  education_1989_revision VARCHAR,
  education_2003_revision VARCHAR,
  education_reporting_flag VARCHAR,
  month_of_death INT,
  sex VARCHAR NOT NULL,
  detail_age_type INT,
  detail_age INT,
  age_substitution_flag VARCHAR,
  age_recode_52 INT,
  age_recode_27 INT,
  age_recode_12 INT,
  infant_age_recode_22 VARCHAR,
  place_of_death_and_decedents_status INT,
  marital_status VARCHAR,
  day_of_week_of_death INT,
  current_data_year INT,
  injury_at_work VARCHAR,
  manner_of_death VARCHAR,
  method_of_disposition VARCHAR,
  autopsy VARCHAR,
  activity_code VARCHAR,
  place_of_injury_for_causes_w00_y34_except_y06_and_y07 VARCHAR,
  icd_code_10th_revision VARCHAR,
  -- reversed
  cause_recode_358 INT,
  -- reversed
  cause_recode_113 INT,
  -- reversed
  infant_cause_recode_130 VARCHAR,
  -- reversed
  cause_recode_39 INT,
  number_of_entity_axis_conditions INT,
  entity_condition_1 VARCHAR,
  entity_condition_2 VARCHAR,
  entity_condition_3 VARCHAR,
  entity_condition_4 VARCHAR,
  entity_condition_5 VARCHAR,
  entity_condition_6 VARCHAR,
  entity_condition_7 VARCHAR,
  entity_condition_8 VARCHAR,
  entity_condition_9 VARCHAR,
  entity_condition_10 VARCHAR,
  entity_condition_11 VARCHAR,
  entity_condition_12 VARCHAR,
  entity_condition_13 VARCHAR,
  entity_condition_14 VARCHAR,
  entity_condition_15 VARCHAR,
  entity_condition_16 VARCHAR,
  entity_condition_17 VARCHAR,
  entity_condition_18 VARCHAR,
  entity_condition_19 VARCHAR,
  entity_condition_20 VARCHAR,
  number_of_record_axis_conditions INT,
  record_condition_1 VARCHAR,
  record_condition_2 VARCHAR,
  record_condition_3 VARCHAR,
  record_condition_4 VARCHAR,
  record_condition_5 VARCHAR,
  record_condition_6 VARCHAR,
  record_condition_7 VARCHAR,
  record_condition_8 VARCHAR,
  record_condition_9 VARCHAR,
  record_condition_10 VARCHAR,
  record_condition_11 VARCHAR,
  record_condition_12 VARCHAR,
  record_condition_13 VARCHAR,
  record_condition_14 VARCHAR,
  record_condition_15 VARCHAR,
  record_condition_16 VARCHAR,
  record_condition_17 VARCHAR,
  record_condition_18 VARCHAR,
  record_condition_19 VARCHAR,
  record_condition_20 VARCHAR,
  race INT,
  bridged_race_flag VARCHAR,
  race_imputation_flag VARCHAR,
  race_recode_3 INT,
  race_recode_5 INT,
  hispanic_origin INT,
  hispanic_origin_race_recode INT
);
    """)


def main():
    print('connecting to database')
    connection = get_connection()
    print('creating table')
    with connection.cursor() as cursor:
        create_mortality_table(cursor)
    print('table created')

    unfiltered_csv_filenames = find_filenames(CLEANED_DATA_PATH)
    csv_filenames = filter_by_year(filenames=unfiltered_csv_filenames)

    for csv_file in csv_filenames:
        print(f"inserting {csv_file}")
        with open(CLEANED_DATA_PATH + csv_file, 'r') as f:
            with connection.cursor() as cursor:
                cursor.copy_from(f, 'mortality', sep=',')
    return 0


if __name__ == '__main__':
    main()
