init:
	# Creating data directories
	mkdir -p file_definitions raw_data zip_files cleaned_data

	# download all zip files
	wget -O zip_files/mort2015us.zip ftp://ftp.cdc.gov/pub/Health_Statistics/NCHS/Datasets/DVS/mortality/mort2015us.zip
	wget -O zip_files/mort2014us.zip ftp://ftp.cdc.gov/pub/Health_Statistics/NCHS/Datasets/DVS/mortality/mort2014us.zip
	wget -O zip_files/mort2013us.zip ftp://ftp.cdc.gov/pub/Health_Statistics/NCHS/Datasets/DVS/mortality/mort2013us.zip
	wget -O zip_files/mort2012us.zip ftp://ftp.cdc.gov/pub/Health_Statistics/NCHS/Datasets/DVS/mortality/mort2012us.zip
	wget -O zip_files/mort2011us.zip ftp://ftp.cdc.gov/pub/Health_Statistics/NCHS/Datasets/DVS/mortality/mort2011us.zip
	wget -O zip_files/mort2010us.zip ftp://ftp.cdc.gov/pub/Health_Statistics/NCHS/Datasets/DVS/mortality/mort2010us.zip
	wget -O zip_files/mort2009us.zip ftp://ftp.cdc.gov/pub/Health_Statistics/NCHS/Datasets/DVS/mortality/mort2009us.zip
	wget -O zip_files/mort2008us.zip ftp://ftp.cdc.gov/pub/Health_Statistics/NCHS/Datasets/DVS/mortality/mort2008us.zip
	wget -O zip_files/mort2007us.zip ftp://ftp.cdc.gov/pub/Health_Statistics/NCHS/Datasets/DVS/mortality/mort2007us.zip
	wget -O zip_files/mort2006us.zip ftp://ftp.cdc.gov/pub/Health_Statistics/NCHS/Datasets/DVS/mortality/mort2006us.zip
	wget -O zip_files/mort2005us.zip ftp://ftp.cdc.gov/pub/Health_Statistics/NCHS/Datasets/DVS/mortality/mort2005us.zip

	# download pdfs
	wget -O file_definitions/Multiple_Cause_RecordLayout_2015.pdf https://www.cdc.gov/nchs/data/dvs/Multiple_Cause_Record_Layout_2015.pdf
	wget -O file_definitions/Record_Layout_2014.pdf https://www.cdc.gov/nchs/data/dvs/Record_Layout_2014.pdf
	wget -O file_definitions/Record_Layout_2013.pdf https://www.cdc.gov/nchs/data/dvs/Record_Layout_2013.pdf
	wget -O file_definitions/Record_Layout_2012.pdf https://www.cdc.gov/nchs/data/dvs/Record_Layout_2012.pdf
	wget -O file_definitions/Record_Layout_2011.pdf https://www.cdc.gov/nchs/data/dvs/Record_Layout_2011.pdf
	wget -O file_definitions/Record_Layout_2010.pdf https://www.cdc.gov/nchs/data/dvs/Record_Layout_2010.pdf
	wget -O file_definitions/Record_Layout_2009.pdf https://www.cdc.gov/nchs/data/dvs/Record_Layout_2009.pdf
	wget -O file_definitions/Record_Layout_2008.pdf https://www.cdc.gov/nchs/data/dvs/Record_Layout_2008.pdf
	wget -O file_definitions/Record_Layout_2007.pdf https://www.cdc.gov/nchs/data/dvs/Record_Layout_2007.pdf
	wget -O file_definitions/Record_Layout_2006.pdf https://www.cdc.gov/nchs/data/dvs/Record_Layout_2006.pdf
	wget -O file_definitions/Record_Layout_2005.pdf https://www.cdc.gov/nchs/data/dvs/Record_Layout_2005.pdf

	# create python virtualenvironment
	mkvirtuatualenv -a `pwd` cdc-data-parser
	workon cdc-data-parser

	# install python dependencies
	pip install -r requirements.txt -r requirements_dev.txt

coverage: ## check code coverage quickly with the default Python
	coverage run --source cdc_data_parser -m pytest
	coverage report -m
	coverage html
	$(BROWSER) htmlcov/index.html

test:
	pytest
