FROM rappdw/docker-java-python
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
ADD . .
RUN pip install -r requirements.txt
