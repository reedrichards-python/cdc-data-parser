from unittest import mock

import pytest

from cdc_data_parser.cleaned_data_to_postgres import (filter_by_year,
                                                      find_filenames)


@pytest.mark.parametrize(
    "filenames,years,filtered_filenames",
    [
        [
            ['2005', '2006', '2007'],
            ['2006', '2007'],
            ['2005']
        ],
        [
            ['2005_data.csv', '2006_data.csv', '2007_data.csv'],
            ['2006_data.csv', '2007_data.csv'],
            ['2005_data.csv']
        ]
    ],
)
def test_filter_by_year(filenames, years, filtered_filenames):
    assert filter_by_year(filenames, years) == filtered_filenames


@mock.patch('cdc_data_parser.cleaned_data_to_postgres.listdir')
@pytest.mark.parametrize(
    "file_list,suffix,found_files",
    [
        [
            ['data.csv', 'data.json', 'data.txt'],
            '.csv',
            ['data.csv']
        ],
        [
            ['data.csv', 'data.json', 'data.txt'],
            '.txt',
            ['data.txt']
        ],
    ],
)
def test_find_filenames(mock_listdir, file_list, suffix, found_files):
    mock_listdir.return_value = file_list
    assert find_filenames(path_to_dir='test',
                          suffix=suffix) == found_files
